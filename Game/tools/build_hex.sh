#!/bin/bash

function build_hexfiles {
    CODE_WORDS=$1
    DATA_WORDS=$2
    CODE_FILE=$3
    DATA_FILE=$4

    elf2mif $file .code.mif .data.mif $CODE_WORDS $DATA_WORDS
    echo "v2.0 raw" > $CODE_FILE
    cat .code.mif >> $CODE_FILE

    echo "v2.0 raw" > $DATA_FILE
    cat .data.mif >> $DATA_FILE

    rm -f .code.mif .data.mif
}

GAS="/opt/gcc-mips-7.1.0/bin/mips-elf-as"
CODE_WORDS=1024
DATA_WORDS=2028
OUTPUT_DIR=$1
shift

mkdir -p $OUTPUT_DIR 2>/dev/null
if [ $? -ne 0 ]; then
    echo "Error creating output directory $OUTPUT_DIR"
    exit
fi

for file in $*; do
    if [[ -e $file ]]; then
        echo Building file $1 ...
        BASENAME=${file//.elf/}
        CODE_FILE="$OUTPUT_DIR/${BASENAME}_code.hex"
        DATA_FILE="$OUTPUT_DIR/${BASENAME}_data.hex"
        
        build_hexfiles $CODE_WORDS $DATA_WORDS $CODE_FILE $DATA_FILE
    else
        echo "File $file doesn't exists"
    fi
done



