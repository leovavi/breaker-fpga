#ifndef _GAME_C
#define _GAME_C

#include "game.h"

void render(){
	if(state == 1){
		if(!bgSet){
			setBackgroundStart();
			bgSet = true;
		}
		set_cursor(10, 37);
		puts("PONG GAME");
		set_cursor(15, 30);
		puts("Press any key to Start");
	}
	else if(state == 2){
		if(!bgSet){
			setBackgroundGame();
			bgSet = true;
		}
		draw_kills();
		drawPalette();
		drawBlocks();
		if(ballShot){
			draw_ball();
			ball_collisions();
		}

		if(kills == 10){
			state = 3;
			bgSet = false;
		}
	}
	else if(state == 3){
		if(!bgSet){
			setBackgroundGO();
			bgSet = true;
		}
		set_cursor(10, 37);
		puts(kills < 10 ? "GAME OVER" : " YOU WON ");
		set_cursor(15, 37);
		puts("Kills: ");
		put_char(kills+48);
		set_cursor(18, 25);
		puts("Press Any Button to Play Again");
	}

	uint8_t key = keypad_getkey();

	switch(key){
		case 0: 
			break;

		case 1:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;

		case 2:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;

		case 3:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;

		case 4:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;

		case 5:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;

		case 6:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;

		case 7:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;

		case 8:
			if(state == 1){
				state = 2;
				bgSet = false;	
			}else if(state == 3){
				state = 2;
				resetDefaults();
			}
			break;
	}
}

void setBackgroundStart(){
	set_color(BRIGHT_WHITE, BROWN);
	clear_screen();
}

void setBackgroundGame(){
	set_color(BRIGHT_WHITE, CYAN);
	clear_screen();
}

void setBackgroundGO(){
	set_color(BRIGHT_WHITE, GRAY);
	clear_screen();
}

void draw_kills(){
	set_color(RED, CYAN);
	set_cursor(0, 0);
	puts(" Kills: ");
	put_char(kills+48);
}

void drawPalette(){
	set_cursor(palette_r, palette_c);
	set_color(BRIGHT_WHITE, CYAN);
	int i, j;

	for(i = palette_r; i<palette_r+palette_h; i++){
		set_cursor(i, palette_c);
		for(j = palette_c; j<palette_c+palette_w; j++){
			put_char(' ');
		}
	}

	uint8_t key = keypad_getkey();

	if(key == 1){
		if(palette_c-2 >= 0){
			palette_c -= 2;

			if(!ballShot){
				set_cursor(ball_r, ball_c);
				set_color(BRIGHT_WHITE, CYAN);
				put_char(' ');
				ball_c -= 2;
				set_cursor(ball_r, ball_c);
				set_color(CYAN, BRIGHT_WHITE);
				put_char(255);
			}
		}
	}
	else if(key == 2){
		if(palette_c+palette_w+2 < 80){
			palette_c += 2;

			if(!ballShot){
				set_cursor(ball_r, ball_c);
				set_color(BRIGHT_WHITE, CYAN);
				put_char(' ');
				ball_c += 2;
				set_cursor(ball_r, ball_c);
				set_color(CYAN, BRIGHT_WHITE);
				put_char(255);
			}
		}
	}else if(key == 6){
		if(!ballShot)
			ballShot = true;
	}

	set_color(BRIGHT_WHITE, LIGHT_MAGENTA);

	for(i = palette_r; i<palette_r+palette_h; i++){
		set_cursor(i, palette_c);
		for(j = palette_c; j<palette_c+palette_w; j++){
			put_char(254);
		}
	}

	delay_ms(100);
}

void draw_ball(){
	set_cursor(ball_r, ball_c);
	set_color(BRIGHT_WHITE, CYAN);
	put_char(' ');

	ball_c = ball_x == 1 ? ball_c+1 : ball_c-1;
	ball_r = ball_y == 1 ? ball_r+1 : ball_r-1;

	set_cursor(ball_r, ball_c);
	set_color(CYAN, BRIGHT_WHITE);
	put_char(255);

	delay_ms(20);
}

void ball_collisions(){
	if(ball_r+1 == palette_r && ball_c >= palette_c && ball_c <= palette_c+palette_w){
		ball_y = -1;
	}

	int i;

	for(i = 0; i<5; i++){
		if(blocks[i][2] == 1){
			if(ball_c+1 == blocks[i][1] && ball_r >= blocks[i][0] && ball_r <= blocks[i][0]+block_h){
				ball_x = -1;
				blocks[i][2] = 0;
				kills++;
			}
			else if(ball_c == blocks[i][1]+block_w && ball_r >= blocks[i][0] && ball_r <= blocks[i][0]+block_h){
				ball_x = 1;
				blocks[i][2] = 0;
				kills++;
			}
			else if(ball_r == blocks[i][0]+block_h && ball_c >= blocks[i][1] && ball_c <= blocks[i][1]+block_w){
				ball_y = 1;
				blocks[i][2] = 0;
				kills++;
			}
			else if(ball_r+1 == blocks[i][0] && ball_c >= blocks[i][1] && ball_c <= blocks[i][1]+block_w){
				ball_y = -1;
				blocks[i][2] = 0;
				kills++;
			}
		}

		if(blocks2[i][2] == 1){
			if(ball_c+1 == blocks2[i][1] && ball_r >= blocks2[i][0] && ball_r <= blocks[i][0]+block_h){
				ball_x = -1;
				blocks2[i][2] = 0;
				kills++;
			}
			else if(ball_c == blocks2[i][1]+block_w && ball_r >= blocks2[i][0] && ball_r <= blocks2[i][0]+block_h){
				ball_x = 1;
				blocks2[i][2] = 0;
				kills++;
			}
			else if(ball_r == blocks2[i][0]+block_h && ball_c >= blocks2[i][1] && ball_c <= blocks2[i][1]+block_w){
				ball_y = 1;
				blocks2[i][2] = 0;
				kills++;
			}
			else if(ball_r+1 == blocks2[i][0] && ball_c >= blocks2[i][1] && ball_c <= blocks2[i][1]+block_w){
				ball_y = -1;
				blocks2[i][2] = 0;
				kills++;
			}
		}
	}

	if(ball_c <= 0) ball_x = 1;
	else if(ball_c+1 >= 79) ball_x = -1;

	if(ball_r <= 0) ball_y = 1;
	else if(ball_r+1 == 30){
		state = 3;
		bgSet = false;
	}
}

void resetDefaults(){
	palette_r = 28;
	palette_c = 35;
	palette_w = 10;
	palette_h = 1;

	ball_r = 27;
	ball_c = 40;
	ball_x = 1;
	ball_y = -1;
	ballShot = false;

	bgSet = false;
	kills = 0;
	initBlocks();
}

void drawBlocks(){
	uint8_t b_r;
	uint8_t b_c;

	int i, j, k, erase = 0;

	for(k = 0; k<5; k++){
		set_color(BRIGHT_WHITE, blocks[k][3]);
		erase = 0;

		b_r = blocks[k][0];
		b_c = blocks[k][1];

		if(blocks[k][2] == 0)
			erase = 1;

		if(erase == 1)
			set_color(BRIGHT_WHITE, CYAN);
		
		for(i = b_r; i<b_r+block_h; i++){
			set_cursor(i, b_c);
			for(j = b_c; j<b_c+block_w; j++){
				put_char(' ');
			}
		}
	}

	for(k = 0; k<5; k++){
		set_color(BRIGHT_WHITE, blocks2[k][3]);
		erase = 0;

		b_r = blocks2[k][0];
		b_c = blocks2[k][1];

		if(blocks2[k][2] == 0)
			erase = 1;

		if(erase == 1)
			set_color(BRIGHT_WHITE, CYAN);
		
		for(i = b_r; i<b_r+block_h; i++){
			set_cursor(i, b_c);
			for(j = b_c; j<b_c+block_w; j++){
				put_char(' ');
			}
		}
	}
}

void initBlocks(){
	int i, temp = 10;

    for(i = 0; i<5; i++){
        blocks[i][0] = 5;
        blocks[i][1] = temp;
        temp += block_w+5;
        blocks[i][2] = 1;
        blocks[i][3] = colors[i];
    }

    temp = 5;

    for(i = 0; i<5; i++){
        blocks2[i][0] = 10;
        blocks2[i][1] = temp;
        temp += block_w+5;
        blocks2[i][2] = 1;
        blocks2[i][3] = colors[i];
    }
}

#endif /* _GAME_C */