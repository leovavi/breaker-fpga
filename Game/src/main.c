#include "game.h"

void mistery();
void resetTile();
void drawTile();
void moveTile();

int main() {
    keypad_init();
    bool playing = true;
    state = 1;
    bgSet = false;
    
    palette_r = 28;
    palette_c = 35;
    palette_w = 10;
    palette_h = 1;

    ball_r = 27;
    ball_c = 40;
    ball_x = 1;
    ball_y = -1;
    ballShot = false;

    block_w = 8;
    block_h = 3;

    colors[0] = LIGHT_BLUE;
    colors[1] = LIGHT_GREEN;
    colors[2] = RED;
    colors[3] = BROWN;
    colors[4] = MAGENTA;

    initBlocks();

    while(playing){
        render();
    }

	return 0;
}
