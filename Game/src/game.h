#ifndef _GAME_H
#define _GAME_H

#include "system.h"
#include "screen.h"
#include "keypad.h"

#define START_ROW 0
#define START_COL 22

uint8_t palette_h;
uint8_t palette_w;
uint8_t palette_r;
uint8_t palette_c;

uint8_t ball_r;
uint8_t ball_c;
uint8_t ball_x;
uint8_t ball_y;
bool ballShot;

uint8_t state;
uint8_t kills;

uint8_t blocks[5][4];
uint8_t blocks2[5][4];

uint8_t colors[5];

uint8_t block_w;
uint8_t block_h;

bool bgSet;

void render();
void setBackgroundStart();
void setBackgroundGame();
void setBackgroundGO();
void draw_kills();
void drawPalette();
void draw_ball();
void ball_collisions();
void resetDefaults();

void drawBlocks();
void initBlocks();
#endif /* _GAME_H */
