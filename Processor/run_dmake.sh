#!/bin/bash

DMAKE=./tools/dmake
OUTPUT_DIR=build-dig
OUTPUT_LIB=mips32
CPUSRC=vsrc/cpu
VGASRC=vsrc/vgacolor

# -t option generate the UPC template files
$DMAKE -t -D $OUTPUT_DIR -l $OUTPUT_LIB \
        -sc ../verilator/VGATextWindow.cpp \
        -CFLAGS "-I/usr/include/SDL2 -I../verilator" \
        -LDFLAGS "-lSDL2" \
        $CPUSRC/MRDD.v \
        $CPUSRC/MWDE.v \
        $CPUSRC/MsCounter.v \
        $CPUSRC/ClockM.v \
        $VGASRC/VGATextCard.v \
        $CPUSRC/BranchResolver.v \
        $CPUSRC/SignExtender.v \
        $CPUSRC/ControlUnit.v \
        $CPUSRC/InstMem.v \
        $CPUSRC/DataMem.v \
        $CPUSRC/IO.v \
        $CPUSRC/Alu.v \
        $CPUSRC/RegFile.v \
        $CPUSRC/PCDecoder.v \
        $CPUSRC/MemoryDecoder.v \
        
                                        
