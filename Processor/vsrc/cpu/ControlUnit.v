`include "opcodes.vh" 

module ControlUnit(
    input [5:0] op, //! Opcode
    input [5:0] fn, //! Function
    input [4:0] br, //! Branch is BLTZ(0) or BGEZ(1)
    output reg jmp, //! Jump signal
    output reg beq, //! BEQ signal
    output reg bne, //! BNE signal
    output reg memr, //! Memory Read
    output reg [1:0] m2r, //! Register File write data selection
    output reg memw, //! Memory Write
    output reg [1:0]asrc, //! ALU source
    output reg regw, //! Register Write
    output reg [1:0] regd, //! Register Destination Address selection
    output reg [3:0] aop, //! ALU operation
    output reg sze, //!Sign or Zero Extend
    output reg iop, //! Invalid Opcode
    output reg [1:0]mds, //! Mem Data Size
    output reg shft, //! Shift Operation
    output reg bgez, //! Branch Greater or Equal to Zero
    output reg bgtz, //! Branch Greater Than Zero
    output reg blez, //! Branch Less or Equal to Zero
    output reg bltz, //! Branch Less Than Zero
    output reg jr, //! Jump to Register
    output reg jal //! Jump and Link
);

always @ (op or fn)
begin
    jmp = 1'b0;
    beq = 1'b0;
    bne = 1'b0;
    memr = 1'b0;
    m2r = 2'b0;
    memw = 1'b0;
    asrc = 2'b0;
    regw = 1'b0;
    regd = 2'b00;
    aop = 4'b0000;
    sze = 1'b1;
    mds = 2'd0;
    iop = 1'b1;
    shft = 1'b0;
    {bgez, bgtz, blez, bltz, jr, jal} = {6{1'b0}};

    case (op)
        `NOP:
            case (fn)
                `SLL: 
                    begin
                        shft = 1'b1;
                        asrc = 2'd2;
                        regd = 2'b01;
                        regw = 1'b1;
                        aop = 4'b1001;
                        iop = 1'b0;
                    end
                `SLLV:
                    begin
                        shft = 1'b1;
                        asrc = 2'd3;
                        regw = 1'b1;
                        regd = 2'b01;
                        aop = 4'b1001;
                        iop = 1'b0;
                    end
                `SRL:
                    begin
                        shft = 1'b1;
                        asrc = 2'd2;
                        regd = 2'b01;
                        regw = 1'b1;
                        aop = 4'b1010;
                        iop = 1'b0;
                    end
                `SRLV:
                    begin
                        shft = 1'b1;
                        asrc = 2'd3;
                        regd = 2'b01;
                        regw = 1'b1;
                        aop = 4'b1010;
                        iop = 1'b0;
                    end
                `ADD: 
                    begin
                        regw = 1'b1;
                        asrc = 2'b0;
                        regd = 2'b01;
                        m2r = 2'b0;
                        aop = 4'b0000;
                        iop = 1'b0;
                    end
                `ADDU: 
                    begin
                        regw = 1'b1;
                        asrc = 2'b0;
                        regd = 2'b01;
                        m2r = 2'b0;
                        aop = 4'b0000;
                        iop = 1'b0;
                    end
                `SUB: 
                    begin
                        regw = 1'b1;
                        asrc = 2'b0;
                        regd = 2'b01;
                        m2r = 2'b0;
                        aop = 4'b0001;
                        iop = 1'b0;
                    end
                `SUBU:
                    begin
                        regw = 1'b1;
                        asrc = 2'b0;
                        regd = 2'b01;
                        m2r = 2'b0;
                        aop = 4'b0001;
                        iop = 1'b0;
                    end
                `AND: 
                    begin
                        regw = 1'b1;
                        asrc = 2'b0;
                        regd = 2'b01;
                        m2r = 2'b0;
                        aop = 4'b0100;
                        iop = 1'b0;
                    end
                `OR: 
                    begin
                        regw = 1'b1;
                        asrc = 2'b0;
                        regd = 2'b01;
                        m2r = 2'b0;
                        aop = 4'b0011;
                        iop = 1'b0;
                    end
                `SLT: 
                    begin
                        regw = 1'b1;
                        asrc = 2'b0;
                        regd = 2'b01;
                        m2r = 2'b0;
                        aop = 4'b0111;
                        iop = 1'b0;
                    end
                `SLTU:
                    begin
                        regw = 1'b1;
                        regd = 2'b01;
                        aop = 4'b1000;
                        iop = 1'b0;
                    end
                `XOR:
                    begin
                        regw = 1'b1;
                        regd = 2'b01;
                        aop = 4'b0101;
                        iop = 1'b0;
                        sze = 1'b0;
                    end
                `SRA:
                    begin
                        shft = 1'b1;
                        asrc = 2'd2;
                        regd = 2'd1;
                        regw = 1'b1;
                        aop = 4'b1011;
                        iop = 1'b0;
                    end
                `SRAV:
                    begin
                        shft = 1'b1;
                        asrc = 2'd3;
                        regd = 2'd1;
                        regw = 1'b1;
                        aop = 4'b1011;
                        iop = 1'b0;
                    end
                `JR: {jr, iop} = {1'b1, 1'b0};
                default: begin end
            endcase
        `ADDI: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'b00;
                m2r = 2'b0;
                aop = 4'b0000;
                iop = 1'b0;
                end
        `ADDIU: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'b00;
                m2r = 2'b0;
                aop = 4'b0000;
                iop = 1'b0;
            end
        `ORI: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'b00;
                m2r = 2'b0;
                aop = 4'b0011;
		        sze = 1'b0;
                iop = 1'b0;
            end
        `XORI:
            begin
                regw = 1'b1;
                asrc = 2'b1;
                aop = 4'b0101;
                iop = 1'b0;
                sze = 1'b0;
            end
        `ANDI: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'b00;
                m2r = 2'b0;
                aop = 4'b0100;
		        sze = 1'b0;
                iop = 1'b0;
            end
        `SLTI:
            begin
                regw = 1'b1;
                asrc = 2'b1;
                aop = 4'b0111;
                iop = 1'b0;
            end
        `SLTIU:
            begin
                regw = 1'b1;
                asrc = 2'b1;
                aop = 4'b1000;
                iop = 1'b0;
            end
        `LW:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                iop = 1'b0;
            end
        `LH:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mds = 2'd1; 
                iop = 1'b0;
            end
        `LHU:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mds = 2'd1; 
                sze = 1'd0;
                iop = 1'b0;
            end
        `LB:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mds = 2'd2;
                iop = 1'b0;
            end
        `LBU:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mds = 2'd2; 
                sze = 1'd0;
                iop = 1'b0;
            end
        `LUI:
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'b00;
                m2r = 2'b10;
                iop = 1'b0;
            end
        `SW:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                memw = 'b1;
                aop = 4'b0000;
                iop = 1'b0;
            end
        `SH:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                memw = 'b1;
                aop = 4'b0000;
                mds = 2'd1;
                iop = 1'b0;
            end
        `SB:
            begin
                asrc = 2'b1;
                regd = 2'b00;
                memw = 'b1;
                aop = 4'b0000;
                mds = 2'd2;
                iop = 1'b0;
            end
        `BEQ:
            begin
                beq = 1'b1;
                aop = 4'b0001;
                iop = 1'b0;
            end
        `BNE:
            begin
                bne = 1'b1;
                aop = 4'b0001;
                iop = 1'b0;
            end
        `BGEZ:
            begin
                bgez = br == 5'd1 ? 1'b1 : 1'b0;
                bltz = br == 5'd1 ? 1'b0 : 1'b1;
                aop = 4'b0001;
                iop = 1'b0;
            end
        `BGTZ: {bgtz, iop, aop} = {1'b1, 1'b0, 4'b0001};
        `BLEZ: {blez, iop, aop} = {1'b1, 1'b0, 4'b0001};
        `JUMP: {jmp, iop, aop} = {1'b1, 1'b0, 4'b0001};
        `JAL: begin
            {jal, iop} = {1'b1, 1'b0};
            m2r = 2'd3;
            regd = 2'd2;
            regw = 1'd1;
        end
        default: begin end
    endcase
end

endmodule