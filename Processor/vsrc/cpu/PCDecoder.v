module PCDecoder(
	input [31:0] vpc,
	output reg [12:0] ppc,
	output reg ipc
);

always @(vpc) begin
	if(vpc >= 32'h400000 && vpc < 32'h402000) begin
		ppc = vpc[12:0];
		ipc = 1'b0;
	end
	else begin
		ppc = 13'hX;
		ipc = 1'b1;
	end
end

endmodule
