module MemoryDecoder(
	input [31:0] va, //!Virtual Address
	input mw, //! Memory Write Request
	input mr, //! Memory Read Request
	output reg [12:0] pa, //! Physical Address
	output reg ima, //! Invalid Memory Address
	output reg[2:0] me, //! Memory Enable
	output reg [1:0] mb //! Memory Bank Access
);

always @(va) begin
	if(va >= 32'h10010000 && va < 32'h10011000) begin
		{ima, pa} = {1'b0, va[12:0]};
		mb = 2'd0;
		me = (mw || mr) ? 3'b001 : 3'b000;
	end

	else if(va >= 32'h7FFFEFFC && va < 32'h7FFFFFFC) begin
		{ima, pa} = {1'b0, va[12:0]-13'hEFFC+13'h1000};
		mb = 2'd0;
		me = (mw || mr) ? 3'b001 : 3'b000;
	end

	else if(va >= 32'hB800 && va <= 32'hCABF) begin
		{ima, pa} = {1'b0, va[12:0]-13'hB800};
		mb = (mw || mr) ? 2'd1 : 2'd0;
		me = (mw || mr) ? 3'b010 : 3'b000;
	end

	else if(va >= 32'hFFFF0000 && va <= 32'hFFFF000F) begin
		{ima, pa} = {1'b0, va[12:0]};
		mb = (mw || mr) ? 2'd2 : 2'd0;
		me = (mw || mr) ? 3'b100 : 3'b000;
	end

	else begin
		{ima, pa} = {(mw || mr) ? 1'b1 : 1'b0, 13'hX};
		{mb, me} = {2'dX, 3'b000};
	end
end

endmodule