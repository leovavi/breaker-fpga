module MWDE(
	input [31:0] id, //!Input Data
	input [1:0]ofs, //! Byte or Half Word offset
	input iwe, //! Write Enable Signal
	input [1:0]ds, //! Data Size to Access
	output reg [31:0]od, //! Encoded Data
	output reg [3:0]owe //! Write Enable Signal
);

always @(iwe or id or ds or ofs) begin
	if(iwe) begin
		case(ds)
			2'd0:
				{od, owe} = {id, 4'b1111};
			2'd1: begin
				if(ofs <= 2'd1) begin
					od =  {id[15:0], {16{1'b0}}};
					owe = 4'b0011;
				end
				else begin 
					od = {{16{1'b0}}, id[15:0]};
					owe = 4'b1100;
				end
			end
			2'd2: begin
				case(ofs)
					2'd0: begin
						od = {id[7:0], {24{1'b0}}};
						owe = 4'b0001; 
					end
					2'd1: begin
						od = {{8{1'b0}}, id[7:0], {16{1'b0}}};
						owe = 4'b0010;
					end
					2'd2: begin
						od = {{16{1'b0}}, id[7:0], {8{1'b0}}};
						owe = 4'b0100;
					end
					2'd3: begin
						od = {{24{1'b0}}, id[7:0]};
						owe = 4'b1000;
					end
				endcase
			end
			default: {od, owe} = {32'd0, 4'd0};
		endcase
	end

	else {od, owe} = {32'd0, 4'd0};
end

endmodule