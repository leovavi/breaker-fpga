module BranchResolver(
	input beq,
	input bne,
	input bgez,
	input bgtz,
	input blez,
	input bltz,
	input zero,
	input sign,
	output reg bt
);

always @(*) begin
	if(beq) bt = zero ? 1'b1 : 1'b0;
	else if(bne) bt = ~zero ? 1'b1 : 1'b0;
	else if(bgez) bt = ~sign ? 1'b1 : 1'b0;
	else if(bgtz) bt = ~zero && ~sign ? 1'b1 : 1'b0;
	else if(blez) bt = zero || sign ? 1'b1 : 1'b0;
	else if(bltz) bt = sign ? 1'b1 : 1'b0;
	else bt = 1'b0;
end

endmodule