module SignExtender (
    input [15:0] in,
    input sze,
    output [31:0] out
);
    assign out = {sze == 1'b0 ? {16{1'b0}} : {16{in[15]}}, in};
endmodule

