/* verilator lint_off UNUSED */
module Mips32soc(
input CLK,
input reset,
input [7:0] btns_state,
output err,
output invAddr,
output [1:0] blue,
output [2:0] green,
output hsync,
output invOp,
output invPC,
output vsync,
output [2:0] red
);

/* Declarations (registers and wires) */
wire [3:0] aluop;
wire bgtz;
wire [1:0] regdst;
wire jr;
wire [1:0] m2r;
wire jmp;
wire memr;
wire [1:0] asrc;
wire beq;
wire blez;
wire mw;
wire jal;
wire bgez;
wire shft;
wire regw;
wire bne;
wire [1:0] mds;
wire bltz;
wire sze;
wire [31:0] inst;
wire cclk;
wire iclk;
wire vclk;
wire [12:0] pcdecoder0_ppc;
wire or0out;
wire [1:0] splitter0Out;
wire branchResolver0_bt;
wire [31:0] r_alu;
wire zero;
wire [31:0] rd2;
wire [31:0] rd1;
wire [4:0] br;
reg [4:0] mux0out;
wire [31:0] ddod;
wire [12:0] memoryDecoder0_pa;
wire [1:0] mb;
wire [2:0] me;
wire [31:0] dataMem0_rd;
wire [31:0] od;
wire [3:0] owe;
wire [31:0] vgatextCard0_rd;
wire [31:0] io0_data;
wire [31:0] msc;
reg [31:0] mux1out;
reg [31:0] pc;
reg [31:0] mux3out;
reg [31:0] mux4out;
wire [31:0] signExtender0_out;
reg [31:0] mux5out;
wire [31:0] pc_4;
reg [31:0] mux2out;
reg mux6out;
wire sel;
wire rst;
wire [7:0] buttons_state;

`ifdef SYNTHESIS
	assign rst = ~reset;
	assign buttons_state = ~btns_state;
`else
	assign rst = reset;
	assign buttons_state = btns_state;
`endif

/* Assign expressions */
assign br = inst[20:16];
assign err = invPC | invAddr | invOp;
assign or0out = jmp | jal;
assign sel = mux6out | rst;
assign pc_4 = pc + 32'h4;
assign splitter0Out = {jr, or0out};

/* Always blocks */
always @ (posedge iclk) begin
  pc <= mux2out;
end
always @ (*) begin
  case (shft)
    1'h0: mux4out = rd1;
    1'h1: mux4out = rd2;
    default: mux4out = 32'hx;
  endcase
end
always @ (*) begin
  case (m2r)
    2'h0: mux3out = r_alu;
    2'h1: mux3out = ddod;
    2'h2: mux3out = {inst[15:0], 16'h0};
    2'h3: mux3out = pc + 32'h4;
    default: mux3out = 32'hx;
  endcase
end
always @ (*) begin
  case (asrc)
    2'h0: mux5out = rd2;
    2'h1: mux5out = signExtender0_out;
    2'h2: mux5out = {27'h0, inst[10:6]};
    2'h3: mux5out = rd1;
    default: mux5out = 32'hx;
  endcase
end
always @ (*) begin
  case (err)
    1'h0: mux6out = 1'h1;
    1'h1: mux6out = 1'h0;
    default: mux6out = 1'hx;
  endcase
end
always @ (*) begin
  case (mb)
    2'h0: mux1out = dataMem0_rd;
    2'h1: mux1out = vgatextCard0_rd;
    2'h2: mux1out = io0_data;
    2'h3: mux1out = 32'h0;
    default: mux1out = 32'hx;
  endcase
end
always @ (*) begin
  case (rst)
    1'h0: begin
      case (splitter0Out)
        2'h0: begin
          case (branchResolver0_bt)
            1'h0: mux2out = pc + 32'h4;
            1'h1: mux2out = pc + 32'h4 + {signExtender0_out[29:0], 2'h0};
            default: mux2out = 32'hx;
          endcase
        end
        2'h1: mux2out = {pc_4[31:28], {inst[25:0], 2'h0}};
        2'h2: mux2out = rd1;
        2'h3: mux2out = 32'h0;
        default: mux2out = 32'hx;
      endcase
    end
    1'h1: mux2out = 32'h400000;
    default: mux2out = 32'hx;
  endcase
end
always @ (*) begin
  case (regdst)
    2'h0: mux0out = br;
    2'h1: mux0out = inst[15:11];
    2'h2: mux0out = 5'h1f;
    2'h3: mux0out = 5'h1;
    default: mux0out = 5'hx;
  endcase
end

/* Instances and always blocks */
/* ClockM
 */
ClockM clockM0(
  .clk(CLK),
  .cclk(cclk),
  .iclk(iclk),
  .vclk(vclk)
);
/* MemoryDecoder
 */
MemoryDecoder memoryDecoder0(
  .mr(memr),
  .mw(mw),
  .va(r_alu),
  .pa(memoryDecoder0_pa),
  .mb(mb),
  .me(me),
  .ima(invAddr)
);
/* MWDE
 */
MWDE mwde0(
  .iwe(mw),
  .id(rd2),
  .ofs(memoryDecoder0_pa[1:0]),
  .ds(mds),
  .od(od),
  .owe(owe)
);
/* DataMem
 * AddrBits=11
 * lastDataFile=/home/leovavi/Documents/UNITEC/OrgaCompu/mips32soc_singlecycle/MIPS32SOC-Part4-tests/vga_kpad-build/vga_kpad_data.hex
 */
DataMem dataMem0(
  .clk(cclk),
  .en(me[0]),
  .addr(memoryDecoder0_pa[12:2]),
  .wd(od),
  .we(owe),
  .rd(dataMem0_rd)
);
/* VGATextCard
 * AddrBits=12
 * lastDataFile=/home/leovavi/Documents/UNITEC/OrgaCompu/mips32soc_singlecycle/MIPS32SOC-Part2/hexfiles/font_rom.hex
 */
VGATextCard vgatextCard0(
  .rst(rst),
  .clk(cclk),
  .en(me[1]),
  .vclk(vclk),
  .addr(memoryDecoder0_pa[12:2]),
  .wd(od),
  .we(owe),
  .r(red),
  .b(blue),
  .rd(vgatextCard0_rd),
  .g(green),
  .hs(hsync),
  .vs(vsync)
);
/* MsCounter
 */
MsCounter msCounter0(
  .rst(rst),
  .clk(cclk),
  .out(msc)
);
/* IO
 */
IO io0(
  .keys(buttons_state),
  .ms(msc),
  .en(me[2]),
  .addr(memoryDecoder0_pa[12:2]),
  .data(io0_data)
);
/* MRDD
 */
MRDD mrdd0(
  .bitX(sze),
  .id(mux1out),
  .ofs(memoryDecoder0_pa[1:0]),
  .ds(mds),
  .od(ddod)
);
/* RegFile
 * AddrBits=5
 * lastDataFile=/home/leovavi/Documents/UNITEC/OrgaCompu/mips32soc_singlecycle/MIPS32SOC-Parte1-Tests/hex/RegFile.hex
 */
RegFile regFile0(
  .ra2(br),
  .c(iclk),
  .ra1(inst[25:21]),
  .wa(mux0out),
  .wd(mux3out),
  .we(regw),
  .rd2(rd2),
  .rd1(rd1)
);
/* SignExtender
 */
SignExtender signExtender0(
  .in(inst[15:0]),
  .sze(sze),
  .out(signExtender0_out)
);
/* Alu
 */
Alu alu0(
  .op(aluop),
  .a(mux4out),
  .b(mux5out),
  .r(r_alu),
  .z(zero)
);
/* BranchResolver
 */
BranchResolver branchResolver0(
  .zero(zero),
  .blez(blez),
  .bgez(bgez),
  .bgtz(bgtz),
  .bne(bne),
  .sign(r_alu[31]),
  .bltz(bltz),
  .beq(beq),
  .bt(branchResolver0_bt)
);
/* PCDecoder
 */
PCDecoder pcdecoder0(
  .vpc(mux2out),
  .ppc(pcdecoder0_ppc),
  .ipc(invPC)
);
/* InstMem
 * AddrBits=11
 * lastDataFile=/home/leovavi/Documents/UNITEC/OrgaCompu/mips32soc_singlecycle/MIPS32SOC-Part4-tests/vga_kpad-build/vga_kpad_code.hex
 */
InstMem instMem0(
  .clk(iclk),
  .en(sel),
  .addr(pcdecoder0_ppc[12:2]),
  .dout(inst)
);
/* ControlUnit
 */
ControlUnit controlUnit0(
  .br(inst[20:16]),
  .op(inst[31:26]),
  .fn(inst[5:0]),
  .aop(aluop),
  .bgtz(bgtz),
  .regd(regdst),
  .jr(jr),
  .iop(invOp),
  .m2r(m2r),
  .jmp(jmp),
  .memr(memr),
  .asrc(asrc),
  .beq(beq),
  .blez(blez),
  .memw(mw),
  .jal(jal),
  .bgez(bgez),
  .shft(shft),
  .regw(regw),
  .bne(bne),
  .mds(mds),
  .bltz(bltz),
  .sze(sze)
);

endmodule
