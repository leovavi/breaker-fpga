module MRDD(
	input [31:0]id, //! Input Data
	input [1:0]ofs, //! Byte or Half Word Offset
	input bitX, //! Sign or Zero Extend
	input [1:0]ds, //! Data Size
	output reg [31:0]od //! Decoded Data
);

always @(id or ofs or bitX or ds) begin
	case(ds)
		2'd0: od = id;
		2'd1: begin
			if(ofs <= 2'd1) od = {bitX ? {16{id[31]}} : {16{1'b0}}, id[31:16]};
			else od = {bitX ? {16{id[15]}} : {16{1'b0}}, id[15:0]};
		end
		2'd2: begin
			case(ofs)
				2'd0: od = {bitX ? {24{id[31]}} : {24{1'b0}}, id[31:24]};
				2'd1: od = {bitX ? {24{id[23]}} : {24{1'b0}}, id[23:16]};
				2'd2: od = {bitX ? {24{id[15]}} : {24{1'b0}}, id[15:8]};
				2'd3: od = {bitX ? {24{id[7]}} : {24{1'b0}}, id[7:0]};
			endcase
		end
		default: od = 32'dX;
	endcase
end

endmodule