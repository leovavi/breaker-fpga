module IO(
	input [10:0] addr,
	input en,
	input [7:0] keys,
	input [31:0] ms,
	output reg [31:0] data
);

always @(addr or en or keys or ms) begin
	if(en)
		begin
			if(addr == 11'd1) data = {keys, 24'd0};
			else if(addr == 11'd2) data = ms;
			else data = 32'dx;
		end
	else
		data = 32'dx;
end

endmodule